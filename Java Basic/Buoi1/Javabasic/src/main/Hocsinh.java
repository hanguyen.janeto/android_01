package main;

public class Hocsinh extends HoatDong{
	private  boolean coDiHoc;
	private String lop;
	public String getLop () {
		return lop;
	}
	public void setLop(String lop) {
		this.lop = lop;
	}
	public void diHoc() {
		coDiHoc = true;
	}
	public boolean coMat() {
		return coDiHoc;
	}
}
