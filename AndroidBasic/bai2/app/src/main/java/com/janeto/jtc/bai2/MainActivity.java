package com.janeto.jtc.bai2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ListView listView;
    private Button addBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();

    }
    private void initView(){
        listView = findViewById(R.id.listView);
        addBtn = findViewById(R.id.buttonAdd);
        final List<Student> studentList = new ArrayList<>(0);
        for(int i = 0; i< 100; i++){
            studentList.add(new Student("student" + i,i + "",18));
        }
        final CustomAdapter customAdapter = new CustomAdapter(this,0,studentList,this.getLayoutInflater());
         listView.setAdapter(customAdapter);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                studentList.add(new Student("new student","15",19));
                customAdapter.notifyDataSetChanged();
            }
        });
    }
}
