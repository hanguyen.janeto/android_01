package com.janeto.jtc.bai13;

import android.app.Application;

public class DemoApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        ApiService.init("https://api.github.com");
    }
}
